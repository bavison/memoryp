/* memoryp - inspired by RISC OS's "*Memory P" command */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#define PAGE_SIZE 4096
#define PAGE_MASK (PAGE_SIZE-1)

static int fd;

static volatile void *map(uint64_t target)
{
  static uint64_t          mapped_phys = -1;
  static volatile uint8_t *mapped_virt;
  uint64_t                 base = target &~ (uint64_t) PAGE_MASK;
  if (mapped_phys != base)
  {
    if (mapped_phys != (uint64_t) -1)
      munmap((void *) mapped_virt, PAGE_SIZE);
    if ((mapped_virt = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, base)) == (void *) -1)
    {
      perror("mmap");
      close(fd);
      exit(EXIT_FAILURE);
    }
    mapped_phys = base;
  }
  return mapped_virt + (target & PAGE_MASK);
}

int main(int argc, char *argv[])
{
  enum
  {
    byte,
    halfword,
    doubleword,
    word,
  }
  access_type = word;
  const int access_size[] = { 1, 2, 8, 4 };
  const char *const access_fmt[] = { PRIX8, PRIX16, PRIX64, PRIX32 };
  bool found_base = false, found_size = false;
  uint64_t base;
  uint64_t size = 0x100;
  int arg;

  for (arg = 1; arg < argc; ++arg)
  {
    const char *modifiers = "BbHhDd";
    const char *modifier;
    char *endptr;
    if (arg == 1 && argv[1][0] != '\0' && argv[1][1] == '\0' &&
        (modifier = strchr(modifiers, argv[1][0])) != NULL)
    {
      access_type = (modifier - modifiers) / 2;
    }
    else if (!found_base)
    {
      base = strtoull(argv[arg], &endptr, 16);
      if (*endptr != '\0')
      {
        fprintf(stderr, "Bad hex\n");
        exit(EXIT_FAILURE);
      }
      found_base = true;
    }
    else if (!found_size)
    {
      size = strtoull(argv[arg], &endptr, 16);
      if (*endptr != '\0')
      {
        fprintf(stderr, "Bad hex\n");
        exit(EXIT_FAILURE);
      }
      found_size = true;
    }
    else
    {
      break;
    }
  }

  if (!found_base || arg < argc)
  {
    fprintf(stderr, "Syntax: %s [B|H|D] <base> [<length>]\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  if ((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1)
  {
    perror("/dev/mem");
    exit(EXIT_FAILURE);
  }

  printf("Address    :");
  for (int o = 0; o < 16; ++o)
  {
    char fmt[4];
    sprintf(fmt, "%%%uX", (o & (access_size[access_type] - 1)) == 0 ? access_size[access_type] + 2 : 2);
    printf(fmt, ((int) base + (o ^ (access_size[access_type] - 1))) & 0xF);
  }
  printf(" :    ASCII Data\n");

  while ((int64_t) size > 0)
  {
    unsigned int offset;
    printf("%010" PRIX64 " :", base);
    for (offset = 0; offset < 16; offset += access_size[access_type])
    {
      volatile uint8_t *addr = map(base + offset);
      char fmt[8];
      uint64_t val = 0;
      for (int spc = access_size[access_type]; spc > 0; --spc)
        printf(" ");
      if (offset < size)
      {
        sprintf(fmt, "%%0%u%s", 2 * access_size[access_type], access_fmt[access_type]);
        switch (access_type)
        {
          case byte:       val = *(volatile uint8_t *) addr; break;
          case halfword:   val = *(volatile uint16_t *) addr; break;
          case word:       val = *(volatile uint32_t *) addr; break;
          case doubleword: val = *(volatile uint64_t *) addr; break;
        }
      }
      else
      {
        sprintf(fmt, "%%%uc", 2 * access_size[access_type]);
        val = ' ';
      }
      if (offset < size && access_type == doubleword)
        printf(fmt, val);
      else
        printf(fmt, (long) val);
    }
    printf(" : ");
    for (offset = 0; offset < 16; offset += access_size[access_type])
    {
      volatile uint8_t *addr = map(base + offset);
      if (offset < size)
      {
        uint64_t val = 0;
        switch (access_type)
        {
          case byte:       val = *(volatile uint8_t *) addr; break;
          case halfword:   val = *(volatile uint16_t *) addr; break;
          case word:       val = *(volatile uint32_t *) addr; break;
          case doubleword: val = *(volatile uint64_t *) addr; break;
        }
        for (int c = 0; c < access_size[access_type]; ++c)
        {
          int v = val & 0xFF;
          if (v >= ' ' && v < 127)
            printf("%c", v);
          else
            printf(".");
          val >>= 8;
        }
      }
    }
    printf("\n");
    base += 16;
    size -= 16;
  }

  close(fd);
  exit(EXIT_SUCCESS);
}
